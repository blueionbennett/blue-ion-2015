<?php

$error = FALSE;

if(empty($_GET['first_name']))
{
	$error = TRUE;
}

if(empty($_GET['emailReal']))
{
	$error = TRUE;
}

if ($_GET['emailAlt'] || $_GET['email'])
{
	$error = TRUE;
}

if(!$error)
{

$to = 'info@blueion.com';
$subject = 'BlueIon.com Contact Form Submission';

$body = "First Name: ". $_GET['first_name'] . "\r
Last Name: ". $_GET['last_name'] . "\r
Email: ". $_GET['emailReal'] . "\r
Phone: ". $_GET['phone'] . "\r
Message: ". $_GET['message'];

$header = "From:". $_GET['emailReal'];

mail($to, $subject, $body, $header);

echo "Thanks, your message has been sent successfully!";

}

else {
	echo "Error sending form. Try again.";
}
?>

