/* #Initialize Foundation
================================================== */
$(document).foundation({
	equalizer : {
	    // Specify if Equalizer should make elements equal height once they become stacked.
	    equalize_on_stack: false
    }
});


/* #Preloader
================================================== */
$(window).load(function() { // makes sure the whole site is loaded
	$('#status, .preloader-logo').fadeOut(); // will first fade out the loading animation
	$('#preloader').delay(600).fadeOut('slow'); // will fade out the white DIV that covers the website.
	$('body').addClass('loaded').delay(600).css({'overflow-y':'visible'});
});

/* #Toggles
================================================== */
$('.contact-trigger').on('click', function(){
	$('.contact-form-wrapper').addClass('show');
	$('body').addClass('no-scroll');
});

$('.contact-form-wrapper .close').on('click', function(){
	$('.contact-form-wrapper').removeClass('show');
	$('body').removeClass('no-scroll');
});


/* #smoothScroll
================================================== */
$('.smooth').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			$('html,body').animate({
				scrollTop: target.offset().top - 75}, 1000);
			return false;
		}
	}
});


/* #Home Video Background
================================================== */
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	$('#video').addClass('mobile');
	$('section.intro').addClass('mobile');
}

else {
	$('#video').videoBG({
		mp4:'/assets/video/bi-vid.mp4',
		ogv:'/assets/video/bi-vid.ogv',
		webm:'/assets/video/bi-vid.webm',
		poster:'/assets/video/bi-vid.jpg',
		scale:true,
		zIndex: 0,
		width: '100%',
		height: '100%',
	});
}


/* #SLICK SLIDER
================================================== */
$('.slider').slick({
	slidesToShow: 3,
	slidesToScroll: 3,
	lazyLoad: 'ondemand',
	prevArrow:"<a class='slick-prev'></a>",
	nextArrow:"<a class='slick-next'></a>",
	responsive: [
		{
			breakpoint: 1400,
			settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		}},
		{
			breakpoint: 700,
			settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		}}
	]
});

$('.intro-slider').slick({
	lazyLoad: 'ondemand',
	prevArrow:"<a class='slick-prev'></a>",
	nextArrow:"<a class='slick-next'></a>"
});




/* #Document Ready
================================================== */
$(document).ready(function(){

/* #Open Intro Div
================================================== */
function noscroll() {
	window.scrollTo( 0, 0 );
}
function scroll() {
	window.removeEventListener( 'scroll', noscroll );
}


/* #Initialize Skrollr
================================================== */
skrollr.init();


/* #Offcanvas Nav-2
================================================== */
$('.menu').on('click', function(){
	$('.fullscreen-2').addClass('open');
	$('body').addClass('no-scroll');
});

$('.close').on('click', function(){
	$('.fullscreen-2').removeClass('open');
	$('body').removeClass('no-scroll');
});


$('.portfolio-content-wrapper .row .box').matchHeight({
	byRow:true
});
$('.matchHeight').matchHeight({
	byRow:true
});


$('.ajax-form').parsley().subscribe('parsley:form:validate', function (formInstance) {
	var $form = formInstance.$element;
	var action = $form.attr('action');
	if (formInstance.isValid()) {
		$.get(action, $form.serialize()).done(function(data) {
			$form.find(".response").html(data);

		// trackEvent('Lead', $('title'), $form.find('#emailReal').val() );

		$form.find("button[type='submit']").fadeOut('slow');
	}).fail(function(data) {
		$form.find(".response").addClass('error').html("Could not submit form.");
	});
	formInstance.submitEvent.preventDefault();
	}
});


$(document).on( 'scroll', function(){
	$('.intro').addClass('opened');
	$('.up-icon').addClass('show');

	if($('.intro').hasClass('opened')){
		setTimeout(scroll, 800);
	}
});

$('.curtain-trigger, .trigger-2').on('click', function(){
	$('.intro').toggleClass('opened');
	$('.up-icon').addClass('show');

	if($('.intro').hasClass('opened')){
		window.removeEventListener( 'scroll', noscroll );
		
	}

	else {
		window.addEventListener( 'scroll', noscroll );
		$('.up-icon').removeClass('show');
	}
});


// $('#process_slides').pagepiling({
// menu: '#menu',
// anchors: ['slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'slide6', 'slide7', 'slide8', 'slide9', 'slide10', 'slide11', 'slide12', 'slide13'],
// sectionsColor: ['#3FAEDF', '#3498C3', '#fff', '#3FAEDF', '#3498C3', '#3FAEDF', '#3498C3', '#3FAEDF', '#3498C3', '#3FAEDF', '#3498C3', '#3FAEDF', '#3498C3'],
// navigation: {
// 'position': 'right',
// 'tooltips': ['Slide 1', 'Slide 2', 'Slide 3', 'Slide 4', 'Slide 5', 'Slide 6', 'Slide 7', 'Slide 8', 'Slide 9', 'Slide 10', 'Slide 11', 'Slide 12', 'Slide 13']
// },
// afterRender: function(){
// $('#pp-nav').addClass('custom');
// },
// afterLoad: function(anchorLink, index){
// if(index>1){
// $('#pp-nav').removeClass('custom');
// }else{
// $('#pp-nav').addClass('custom');
// }
// }
// });


/* #Stop Scroll
================================================== */
window.addEventListener( 'scroll', noscroll );


}); //EO Document Ready
