module.exports = function(grunt) {

	"use strict";

	var pkginfo = grunt.file.readJSON("package.json");

	grunt.initConfig({

		pkg: pkginfo,

		sass: {
			dist: {
				options: {
					style: "expanded",
					loadPath: "bower_components/foundation/scss",
					'sourcemap=none': true
				},
				files: {
					'../htdocs/assets/css/build/app.css': '../htdocs/assets/css/src/app.scss'
				}
			}
		},

		autoprefixer: {

			options: {
				browsers: ['last 2 version', 'ie 9']
			},
			dist: {
				src: '../htdocs/assets/css/build/app.css',
				dest: '../htdocs/assets/css/build/app.css'
			}
		},

		pixrem: {
			options: {
				rootvalue: '14px',
				replace: true
			},
			dist: {
				src: '../htdocs/assets/css/build/app.css',
				dest: '../htdocs/assets/css/build/app.css'
			}
		},

		csso: {
			compress: {
				files: {
					'../htdocs/assets/css/build/app.css': ['../htdocs/assets/css/build/app.css']
				}
			}
		},

		copy: {
			main: {
				files: [
					{expand: true, flatten: true, src: ['bower_components/modernizr/modernizr.js'], dest: '../htdocs/assets/js/vendor/', filter: 'isFile'}
				]
			}
		},

		jshint: {
			src: {
				options: {
					jshintrc: ".jshintrc",
					ignores: ["../htdocs/assets/js/build/*.js", "../htdocs/assets/js/vendor/*.js"],
					reporter: require('jshint-stylish')
				},
				src: ["../htdocs/assets/js/src/*.js"]
			}
		},

		concat: {
			ie: {
				options: {
					separator: "\n\n"
				},
				src: [
					"bower_components/selectivizr/selectivizr.js",
					"bower_components/respond/dest/respond.min.js"
				],
				dest: "../htdocs/assets/js/build/ie.min.js"
			},
			dist: {
				options: {
					separator: "\n\n"
				},
				src: [
					// Foundation Vendor
					"bower_components/fastclick/lib/fastclick.js",
					"bower_components/jquery-placeholder/jquery.placeholder.js",
					"bower_components/jquery.cookie/jquery.cookie.js",
					// Foundation Core
					"bower_components/foundation/js/foundation/foundation.js",
					/*"bower_components/foundation/js/foundation/foundation.abide.js",
					"bower_components/foundation/js/foundation/foundation.accordion.js",
					"bower_components/foundation/js/foundation/foundation.alert.js",
					"bower_components/foundation/js/foundation/foundation.clearing.js",*/
					//"bower_components/foundation/js/foundation/foundation.dropdown.js",
					"bower_components/foundation/js/foundation/foundation.equalizer.js",
					/*"bower_components/foundation/js/foundation/foundation.interchange.js",
					"bower_components/foundation/js/foundation/foundation.joyride.js",*/
					"bower_components/foundation/js/foundation/foundation.magellan.js",
					"bower_components/foundation/js/foundation/foundation.offcanvas.js",
					// "bower_components/foundation/js/foundation/foundation.orbit.js",
					"bower_components/foundation/js/foundation/foundation.reveal.js",
					// "bower_components/foundation/js/foundation/foundation.tab.js",
					// "bower_components/foundation/js/foundation/foundation.tooltip.js",
					// "bower_components/foundation/js/foundation/foundation.topbar.js",
					// Custom Vendor
					// "../htdocs/assets/js/vendor/foundation.magellan-OVERRIDE.js",
					"bower_components/jquery-fittext.js/jquery.fittext.js",
					//"../htdocs/assets/js/vendor/jquery.fancybox.js",
					//"../htdocs/assets/js/vendor/jquery.fancybox-media.js",
					//"../htdocs/assets/js/vendor/jquery.fancybox-thumbs.js",
					//"../htdocs/assets/js/vendor/jquery.flexslider.js",
					"bower_components/jquery.smooth-scroll/jquery.smooth-scroll.min.js",
					"bower_components/jquery-number/jquery.number.min.js",
					"bower_components/jquery-waypoints/waypoints.min.js",
					"../htdocs/assets/js/vendor/select2.min.js",
					"../htdocs/assets/js/vendor/jquery.textarea_auto_expand.js",
					"bower_components/parsleyjs/dist/parsley.min.js",
					//"../htdocs/assets/js/src/vendor/hoverintent.js",
					"bower_components/jquery-backstretch/jquery.backstretch.min.js",
					//"../htdocs/assets/js/src/vendor/jquery.animate_from_to-1.0.js",

					// Init
					"../htdocs/assets/js/src/plugins/*.js",
					"../htdocs/assets/js/src/main.js"

					],
				dest: "../htdocs/assets/js/build/scripts.min.js"
			}
		},

		uglify: {
			min: {
				files: {
					"../htdocs/assets/js/vendor/modernizr.js": ["../htdocs/assets/js/vendor/modernizr.js"],
					"../htdocs/assets/js/build/scripts.min.js": ["../htdocs/assets/js/build/scripts.min.js"],
					"../htdocs/assets/js/build/ie.min.js": ["../htdocs/assets/js/build/ie.min.js"]
				}
			}
		},

		watch: {
			options: {
				livereload: false,
				spawn: false
			},
			grunt: {
				options: {
					reload: true,
				},
				files: ['Gruntfile.js'],
				tasks: ["default"]
			},
			markup: {
				files: ["../htdocs/**/*.php"],
			},
			scss: {
				options: {
					livereload: false
				},
				files: ["../htdocs/assets/css/src/*.scss"],
				tasks: ["sass", "autoprefixer", "pixrem"/*, "csso"*/]
			},
			css : {
				files: ["../htdocs/assets/css/build/*.css"],
				tasks: []
			},
			js: {
				files: ["../htdocs/assets/js/src/*.js"],
				tasks: ["jshint", "concat", "uglify"]
			}
		}

	});

	// Load grunt tasks from NPM packages
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks("grunt-contrib-sass");
	grunt.loadNpmTasks('grunt-pixrem');
	grunt.loadNpmTasks('grunt-csso');
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-watch");

	// Register grunt tasks
	grunt.registerTask("default", ["sass", "autoprefixer", "pixrem", "csso", "copy", "concat", "jshint", "uglify", "watch"]);

};